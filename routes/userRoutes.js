const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req,res){
    const users = UserService.getUsers();
    if (users) {
        res.status(200).json(users);
    } else {
        res.status(400).json({
            error: true,
            message: 'No users in db',
        });
    }});
router.get('/:id', function (req,res){
    const id = req.params.id;
    const foundUser = UserService.search({ id });
    if (foundUser) {
        res.json(foundUser);
    } else {
        res.status(404).json({
            error: true,
            message: 'No user with such id',
        });
    }

});
router.post('/',function(req,res, next){
    createUserValid(req, res, next);
    if(res.statusCode===200){
        const result = UserService.create(req.body);
        if (result) {
            res.json(result);
        } else {
            res.status(400).json({
                error: true,
                message: 'Error has occurred',
            });
        }
    }

});
router.put('/:id',function (req,res,next){
    updateUserValid(req, res, next);
    if(res.statusCode===200) {
    const id = req.params.id;
    const userInfo = req.body;
    const updatedUser = UserService.update(id, userInfo);
    if (updatedUser) {
        res.json(updatedUser);
    } else {
        res.status(404).json({
            error: true,
            message: 'No user with such id',
        });
    }
    }
});
router.delete('/:id',function (req,res){
    const id = req.params.id;
    const deletedUser = UserService.delete(id);
    if (deletedUser) {
        res.json(deletedUser);
    } else {
        res.status(404).json({
            error: true,
            message: 'No user with such id',
        });
    }
});

module.exports = router;