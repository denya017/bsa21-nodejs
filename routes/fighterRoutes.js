const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req, res) {
    const fighters = FighterService.getFighters();
    if (fighters) {
        res.json(fighters);
    } else {
        res.json(404).json({
            error: true,
            message: 'Have no fighters',
        });
    }
});


router.get('/:id', function (req, res) {
    const id = req.params.id;
    const foundFighter = FighterService.search({ id });
    if (foundFighter) {
        res.json(foundFighter);
    } else {
        res.status(404).json({ error: true, message: 'No fighter with such id' });
    }
});

router.post('/', function (req, res, next) {
    createFighterValid(req,res,next);
    if(res.statusCode===200) {
        const result = FighterService.create(req.body);
        if (result) {
            res.json(result);
        } else {
            res.status(400).json({
                error: true,
                message: 'Non validation error',
            });
        }
    }
});

router.put('/:id', updateFighterValid, function (req, res, next) {
    updateFighterValid(req, res, next);
    if(res.statusCode===200) {
    const id = req.params.id;
    const fighterInfo = new Fighter(req.body);
    const updatedFighter = FighterService.update(id, fighterInfo);
    if (updatedFighter) {
        res.json(updatedFighter);
    } else {
        res.status(404).json({
            error: true,
            message: 'No fighter with such id',
        });
    }
    }
});

router.delete('/:id', function (req, res) {
    const id = req.params.id;
    const deletedFighter = FighterService.remove(id);
    if (deletedFighter) {
        res.json(deletedFighter);
    } else {
        res.status(404).json({
            error: true,
            message: 'No fighter with such id',
        });
    }
});
module.exports = router;