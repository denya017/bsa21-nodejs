const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    create(user) {
        return UserRepository.create(user);
    }

    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            return null;
        }
        return users;
    }

    update(id, data) {
        const updatedUser = UserRepository.update(id, data);
        if (!updatedUser) {
            return null;
        }
        return updatedUser;
    }

    delete(id) {
        const deleteUser = UserRepository.delete(id);
        if (!deleteUser) {
            return null;
        }
        return deleteUser;
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();