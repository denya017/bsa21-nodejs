const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    if(Object.keys(req.body).length === (Object.keys(user).length-1)) {

        if (!req.body.email.match(/^[\w-\.]+@gmail.com$/g)) {
            res.status(400).json({
                error: true,
                message: 'Invalid email!'
            });
        } else if (!req.body.firstName.match(/^[A-Za-z ]+$/)) {
            res.status(400).json({
                error: true,
                message: 'Invalid first name!'
            });
        } else if (!req.body.lastName.match(/^[A-Za-z ]+$/)) {
            res.status(400).json({
                error: true,
                message: 'Invalid last name!'
            });
        } else if (!req.body.phoneNumber.match(/^\+380[0-9]{9}$/)) {
            res.status(400).json({
                error: true,
                message: 'Invalid phone number!'
            });
        } else if (!req.body.password.match(/^.{3,}$/)) {
            res.status(400).json({
                error: true,
                message: 'Invalid password!'
            });
        } else {
            res.status(200).json({
                message: 'Create-validation done!'
            });
        }
    }
    else {
        console.log('Fill in all the input fields!');
        res.status(400).json({
            error: true,
            message: 'Not all fields are filled in!'
        });
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    
    let buf = 0;
    if (Object.keys(req.body).length < (Object.keys(user).length - 1) && Object.keys(req.body).length > 0) {
        if (req.body.email) {
            if (!req.body.email.match(/^[\w-\.]+@gmail.com$/g)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid email!'
                });
            }
        }
        if(req.body.firstName) {
            if (!req.body.firstName.match(/^[A-Za-z ]+$/)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid first name!'
                });
            }
        }
        if(req.body.lastName) {
            if (!req.body.lastName.match(/^[A-Za-z ]+$/)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid last name!'
                });
            }
        }
        if(req.body.phoneNumber) {
            if (!req.body.phoneNumber.match(/^\+380[0-9]{9}$/)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid phone number!'
                });
            }
        }
        if(req.body.password) {
            if (!req.body.password.match(/^.{3,}$/)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid password!'
                });
            }
        }
        if(buf===0) {
            res.status(200).json({
                message: 'Create-validation done!'
            });
        }
    } else {
        console.log('Fill in the input fields!');
        res.status(400).json({
            error: true,
            message: 'Fields are not filled in!'
        });
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;