const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    if(Object.keys(req.body).length === (Object.keys(fighter).length-2)) {

        if (!req.body.name.match(/^[A-Za-z ]+$/)) {
            res.status(400).json({
                error: true,
                message: 'Invalid name!'
            });
        } else if (!(req.body.power > 0 && req.body.power <= 100)) {
            res.status(400).json({
                error: true,
                message: 'Invalid power! (Must be in [1; 100]!)'
            });
        } else if (!(req.body.defense > 0 && req.body.defense <= 10)) {
            res.status(400).json({
                error: true,
                message: 'Invalid defense! (Must be in [1; 10]!)'
            });
        }
        else {
            req.body.health = 100;
            res.status(200).json({
                message: 'Create-validation done!'
            });
        }
    }
    else {
        console.log('Fill in all the input fields!');
        res.status(400).json({
            error: true,
            message: 'Not all fields are filled in!'
        });
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if(Object.keys(req.body).length < (Object.keys(fighter).length-2) && Object.keys(req.body).length>0) {
        let buf = 0;
        if(req.body.name) {
            if (!req.body.name.match(/^[A-Za-z ]+$/)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid name!'
                });
            }
        }
        if(req.body.power) {
            if (!(req.body.power > 0 && req.body.power <= 100)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid power! (Must be in [1; 100]!)'
                });
            }
        }
        if(req.body.defense) {
            if (!(req.body.defense > 0 && req.body.defense <= 10)) {
                buf++;
                res.status(400).json({
                    error: true,
                    message: 'Invalid defense! (Must be in [1; 10]!)'
                });
            }
        }
        if(buf===0) {
            req.body.health = 100;
            res.status(200).json({
                message: 'Create-validation done!'
            });
        }
    }
    else {
        console.log('Fill in all the input fields!');
        res.status(400).json({
            error: true,
            message: 'Not all fields are filled in!'
        });
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;